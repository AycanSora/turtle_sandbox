trigger aptlyOpportuntity_BU on Opportunity (before update) {
    
    Map<Id,List<OpportunityLineItem>> opps = new Map<Id,List<OpportunityLineItem>>();
    
    for(Opportunity o : trigger.new)
    {
        system.debug('old: ' + trigger.oldMap.get(o.Id).get('util_approval__c'));
        system.debug('new: ' + o.util_approval__c);
        
        if(o.util_approval__c != trigger.oldMap.get(o.Id).get('util_approval__c'))
        {
           if(o.util_approval__c == 'approved' || o.util_approval__c == 'rejected')
           {
               opps.put(o.id, new List<OpportunityLineItem>());
           }
        }
    }
    
    for(Opportunity O : [select id, (select id,util_approval__c from OpportunityLineItems where util_approval__c = 'needs approval') from opportunity where id in :opps.keySet()])
    {
        opps.put(o.id, o.OpportunityLineItems);
    }
        
    List<OpportunityLineItem> olUpdate = new List<OpportunityLineItem>();
    
    for(Opportunity o : trigger.new)
    {
        if(opps.get(o.Id) != null)
        {
            for(OpportunityLineItem ol : opps.get(o.Id))
            {
                ol.util_approval__c = o.util_approval__c;
                olUpdate.add(ol);
            }
        }
    }
    
    if(olUpdate.size() > 0 ) update olUpdate;
    
}