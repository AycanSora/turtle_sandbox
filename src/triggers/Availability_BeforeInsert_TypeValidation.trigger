trigger Availability_BeforeInsert_TypeValidation on Availability__c (before insert) {

    //not possible to add same type availability 
    if(trigger.isInsert) {
        //Map<inventoryID, Map<availability.TYPE, availabilityRecord>>
        Map<Id,Map<String,Availability__c>> availabilityTotal = new Map<Id,Map<String,Availability__c>>(); 
        
        for(Inventory__c inventory : [SELECT Id, (select id, type__c, Planned__c from PlannedSlots__r) FROM Inventory__c]) {            
            Map<String,Availability__c> inventoryAvailability = new Map<String,Availability__c>();
           
            List<Availability__c> tempAvailabilityList = inventory.PlannedSlots__r;           
            for(Availability__c availability : tempAvailabilityList) {
                inventoryAvailability.put(availability.Type__c,availability);
            }    
            availabilityTotal.put(inventory.Id, inventoryAvailability);
        }

        for(Availability__c ava : trigger.new) {
            if(availabilityTotal.get(ava.InventoryItem__c).get(ava.type__c) != null) {
                //ava.addError('The type '+ ava.type__c +' is already available!');
                ava.addError(String.format(System.Label.Availability_BeforeInsert_TypeValidation, new string[] {ava.type__c}));
            }
        }          
    }
}