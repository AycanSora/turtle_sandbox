trigger OppLineItem_AfterInsert_AfterUpdate_BeforeDelete_InventoryBookingOverviewSync on OpportunityLineItem (after insert, after update, before delete) 
{

    static final String STATUS_LOST = 'Lost';
    static final String BOOKING_OVERVIEW_STATUS_BOOKED = 'Booked';

    // OpportunityLineItem DELETION will update related Slots to LOST
    if (trigger.isDelete) {
        final List<Id> deleteIds = new List<Id>(trigger.oldMap.keySet());
        final List<BookingOverview__c> bookingOverviewList = [
                SELECT id 
                FROM BookingOverview__c 
                WHERE OpportunityLineItemId__c 
                IN :deleteIds
        ];
        for (BookingOverview__c bookingOverview : bookingOverviewList) {
            bookingOverview.Status__c = STATUS_LOST;
        }
        update bookingOverviewList;
    }

    // After OppLineItem was created/updated perform upserts on related slots records
    if(trigger.isafter) {
        
        final Set<Id> accountIds = new Set<Id>();
        final Set<Id> inventoryIds = new Set<Id>();
        
        for (OpportunityLineItem oli : trigger.new) {
            
            // prepare Inventory and Account Maps
            inventoryIds.add(oli.IventoryItemtId__c);
            accountIds.add(oli.AccountId__c);
        }
        
        inventoryIds.remove(null);
        accountIds.remove(null);

        final Map<Id, Account> accountMap = new Map<Id,Account>([
                SELECT id, Name, Industry_Area__c,Type 
                FROM Account 
                WHERE id 
                IN :accountIds
        ]);

        final Map<Id, Inventory__c> inventories = new Map<Id, Inventory__c>([
            SELECT 
                Boothsize_in_sqm_booked__c,Boothsize_in_sqm__c, Deadline_Date__c, End_Date__c, Id, Start_Date__c, Total_Costs__c,Total_Gross_Margin__c, Turnover_planned__c, Turnover__c, (
                        SELECT id, Booked__c, Planned__c, Type__c, SponsoringBlacklist__c, No_Blacklist__c FROM PlannedSlots__r
                    ) 
            FROM 
                Inventory__c 
            WHERE id IN :inventoryIds
        ]);

        // Map<eventID/inventoryId, Map<Availability.TYPE, AvailabilityRecord>>
        final Map<Id, Map<String, Availability__c>> availabilitiesTotal = new Map<Id,Map<String,Availability__c>>();

        // Map<eventID, Map<Availability.TYPE, NumberOfBookedSlots>> // FOR ALL TYPES DEFAULTS TO 1, FOR BOOTH ITS sqm
        final Map<Id, Map<String, Double>> bookedSlotsTotal = new Map<Id,Map<String,Double>>();

        // Map<eventID, Map<slot.Type, Map<AccountCharacteristic/"Industry", Set<AccountIds>>>>
        final Map<Id, Map<String, Map<String,Set<Id>>>> eventTypeAccountsCharacteristics = new Map<Id, Map<String, Map<String,Set<Id>>>>();
        
        //Load additional inventory data + availabilities records
        //inital helper maps
        for (Inventory__c e : inventories.values()) {
            eventTypeAccountsCharacteristics.put(e.id, new Map<String, Map<String, Set<Id>>>());
            
            final Map<String, Availability__c> inventoryAvailability = new Map<String, Availability__c>();
            final Map<String, Double> eventSlotsBooked = new Map<String, Double>();

            if (null != e.PlannedSlots__r)
            {
                for(Availability__c availability : e.PlannedSlots__r)
                {
                    inventoryAvailability.put(availability.Type__c, availability);
                    eventSlotsBooked.put(availability.Type__c, availability.Booked__c);
                }    
            }
            availabilitiesTotal.put(e.Id, inventoryAvailability);
            bookedSlotsTotal.put(e.id, eventSlotsBooked);
        }
        system.debug('inventories: ' + inventories);
        system.debug('availabilitiesTotal:' + availabilitiesTotal);
        system.debug('bookedSlotsTotal:' + bookedSlotsTotal);
        
        // Map<OpportunityLineItemId, BookingOverview__c> implying a 1:1 relationship
        final Map<id, BookingOverview__c> existingBookingOverview = new Map<id,BookingOverview__c>();

        for (BookingOverview__c bo : [
            SELECT 
                Account__c,Account__r.Industry_Area__c,AdditionalCosts__c,Costs__c,CreatedById,CreatedDate,InventoryItem__c,GrossMargin__c,Id,
                            ListPrice__c,OpportunityLineItemId__c,Opportunity__c,Product__c,Quantity__c,SalesPrice__c,Status__c,
                            TotalCosts__c,TotalPrice__c,Boothsize_in_sqm__c,Type__c                    
            FROM 
                BookingOverview__c 
            WHERE 
                InventoryItem__c 
            IN 
                :inventories.keyset() AND Status__c = :BOOKING_OVERVIEW_STATUS_BOOKED
        ]) {
            existingBookingOverview.put(bo.OpportunityLineItemId__c, bo);

            system.debug('eventTypeAccountsCharacteristics: ' + eventTypeAccountsCharacteristics);
            
            // WE TAKE THIS FOR GRANTED (see ll. 68)            
            final Map<String, Map<String, Set<Id>>> typeAccountsCharacteristics = eventTypeAccountsCharacteristics.get(bo.InventoryItem__c);
            
            Map<String, Set<Id>> accountCharacteristicIds = typeAccountsCharacteristics.get(bo.Type__c);

            if (accountCharacteristicIds == null) {
                accountCharacteristicIds = new Map<String, Set<Id>>();
                typeAccountsCharacteristics.put(bo.Type__c, accountCharacteristicIds);
            }

            // fill blacklist with accounts that are already BOOKED
            if (bo.Account__r.Industry_Area__c != null) {

                // Characteristics is a multi picklistfield, so each single characteristic is checked
                for (String industrySegment : bo.Account__r.Industry_Area__c.split(';')) {

                   Set<Id> accountIdsForBlacklist = accountCharacteristicIds.get(industrySegment);
                   if (null == accountIdsForBlacklist) {
                       accountIdsForBlacklist = new Set<id>();
                       accountCharacteristicIds.put(industrySegment, accountIdsForBlacklist);
                   } 
                   accountIdsForBlacklist.add(bo.Account__c);
                }
            }
        }  
    
        final List<BookingOverview__c> upsertSlots = new List<BookingOverview__c>();

        for (OpportunityLineItem oli : trigger.new) {
            // Load account and event from helper maps
            final Account a = accountMap.get(oli.AccountId__c);
            final Inventory__c e = inventories.get(oli.IventoryItemtId__c);
            
            // GO ahead if event is found and the product is LimitedPerEvent
            if (e != null && true == oli.LimitedPerEvent__c) {
                //load relevant maps for this line item from helper maps
                final Map<String,Availability__c> inventoryAvailability = availabilitiesTotal.get(oli.IventoryItemtId__c);
                final Map<String,Double> eventSlotsBooked = bookedSlotsTotal.get(oli.IventoryItemtId__c);
                
                if (inventoryAvailability != null && eventSlotsBooked != null) {
                    final Availability__c availablity = inventoryAvailability.get(oli.Type__c);
                    Double bookedSlotQty = eventSlotsBooked.get(oli.Type__c);
                    system.debug('Availability record: '+availablity);
                    system.debug('Booked Slot Quantity: '+bookedSlotQty);
                    
                    if (null != availablity && bookedSlotQty != null) {
                        final Double plannedSlotQty = availablity.Planned__c;
                        
                        // BLACKLISTCHECK: only perform if blacklist for relevant Planned Slot is not switched off
                        if (true != availablity.No_Blacklist__c) {
                            final Set<String> blacklistsToCheck = new Set<String>{oli.Type__c};
                            if(availablity.SponsoringBlacklist__c !=null) {
                                blacklistsToCheck.addAll(availablity.SponsoringBlacklist__c.split(';'));
                            }

                            system.debug('Opp line item Event: '+oli.IventoryItemtId__c);
                            system.debug('Event Type Accounts Characteristics Map: '+eventTypeAccountsCharacteristics);

                            // Map<plannedSlot.TYPE, Map<account.Characteristic, Set<AccountId>>>
                            final Map<String,Map<String,Set<Id>>> eventBlacklist = eventTypeAccountsCharacteristics.get(oli.IventoryItemtId__c);
                            
                            system.debug('eventBlacklist: ' + eventBlacklist);
                            system.debug('Account Industry: ' + a.Industry_Area__c);
                            system.debug('blacklistsToCheck: ' + blacklistsToCheck);
                            if (null != a.Industry_Area__c) {
                                for(String eventtype : blacklistsToCheck) {
                                    final Map<String,Set<Id>> typeBlacklist = eventBlacklist.get(eventtype);
                                    system.debug('typeBlacklist: ' + typeBlacklist);
                                    if(typeBlacklist !=null) {
                                        for(String accountChars : a.Industry_Area__c.split(';')) {
                                            Set<Id> accountIdsForBlacklist = typeBlacklist.get(accountChars);
                                            system.debug('accountIdsForBlacklist: ' + accountIdsForBlacklist);
                                            if(accountIdsForBlacklist != null && accountIdsForBlacklist.size() > 0 && typeBlacklist.keySet().contains(accountChars) && ! accountIdsForBlacklist.contains(a.id)) 
                                            {
                                                //oli.addError('BLACKLIST: A customer with the industry '+ accountChars + ' has already booked a ' + eventtype + ' slot.');
                                                oli.addError(String.format(System.Label.BlackList_Validation_Error, new string[] {accountChars,eventtype}));
                                                break;
                                            }
                                        }                                
                                    }
                                }
                            }
                        }// END OF BLACKLISTCHECK
    
                        //IF BLACKLIST IS PASSED
                        //go ahead only if DeadLineDate is in the future or it's passed and this has been approved
                        //potential pitfall: if turtle wants to implement more than one approval process the util_approval might need different APPROVED STATUS values (e.g. "approved deadline", "approved margin")
                        if (e.Deadline_Date__c >= date.today() || oli.util_approval__c == 'approved') {
                            final BookingOverview__c bookingToUpsert;
                            if (existingBookingOverview.containsKey(oli.id)) {
                                bookingToUpsert = existingBookingOverview.get(oli.id);
                            } else {
                                bookingToUpsert = new BookingOverview__c();
                            }

                            system.debug('opp line item: '+oli.Id);
                            system.debug('opp line item Type: '+oli.Type__c);
                            system.debug('opp line item status: '+oli.Status__c );
                            system.debug('BookedSlot Quantity: '+bookedSlotQty);
                            system.debug('PlannedSlot Quantity: '+plannedSlotQty);
                            system.debug('Booking to upsert id: '+bookingToUpsert.id+' bookingToUpsert Status: '+bookingToUpsert.Status__c);

                            if (bookedSlotQty >= plannedSlotQty &&
                                (oli.Status__c == BOOKING_OVERVIEW_STATUS_BOOKED || bookingToUpsert.Id == null)
                            ) {
                               //oli.addError(oli.Type__c + ' Slots are fully booked for this event already.');
                               oli.addError(String.format(System.Label.FullyBooked_Error, new string[] {oli.Type__c}));
                               break;
                            }
                            
                            if (oli.type__c == 'Booth') {
                                system.debug('oli.boothsize: '+oli.Boothsize_in_sqm__c);
                                system.debug('availability.planned: '+availablity.Planned__c);
                                if (oli.Boothsize_in_sqm__c > availablity.Planned__c) {
                                    //oli.addError(oli.Boothsize_in_sqm__c + ' is grater than planned '+availablity.Planned__c);
                                    oli.addError(String.format(System.Label.PlannedBoothsizeAvailability_Error, new string[] {oli.Boothsize_in_sqm__c.format(),availablity.Planned__c.format()}));
                                } else {
                                    if (oli.Boothsize_in_sqm__c > (availablity.Planned__c - bookedSlotQty)) {
                                       //oli.addError('There is no enough availablity for '+oli.Boothsize_in_sqm__c);
                                        oli.addError(String.format(System.Label.NoEnoughAvailabilityForBoothsize, new string[] {oli.Boothsize_in_sqm__c.format()})); 
                                    } else {
                                        bookingToUpsert.Quantity__c = oli.Boothsize_in_sqm__c;
                                    } 
                                }
                                
                            } else {
                                bookingToUpsert.Quantity__c = oli.Quantity;
                            }
                            // update SLOT fields from OppLineItem
                            bookingToUpsert.Opportunity__c = oli.OpportunityId;
                            bookingToUpsert.Planned_Slot__c = availablity.id;
                            bookingToUpsert.OpportunityLineItemId__c = oli.id;
                            bookingToUpsert.Product__c = oli.Product2Id;
                            bookingToUpsert.InventoryItem__c = oli.IventoryItemtId__c;
                            bookingToUpsert.AdditionalCosts__c = oli.Additional_Costs__c ;
                            bookingToUpsert.GrossMargin__c = oli.Gross_Margin__c;
                            bookingToUpsert.ListPrice__c = oli.ListPrice;
                            bookingToUpsert.SalesPrice__c = oli.UnitPrice;
                            bookingToUpsert.TotalPrice__c = oli.TotalPrice;
                            bookingToUpsert.TotalCosts__c = oli.Total_Costs__c;
                            bookingToUpsert.Boothsize_in_sqm__c = oli.Boothsize_in_sqm__c;
                            bookingToUpsert.Status__c = oli.Status__c;
                            bookingToUpsert.Account__c = oli.AccountId__c;
                            bookingToUpsert.Type__c = oli.Type__c;
                            bookingToUpsert.LimitedPerInventory__c = true;
                            upsertSlots.add(bookingToUpsert);
                            System.debug('###' + bookingToUpsert);
                            
                            // Increase Booked Counter in helper maps ONLY FOR ACTUALLY BOOKED SLOTS
                            if(bookingToUpsert.Status__c == BOOKING_OVERVIEW_STATUS_BOOKED) 
                            {
                                bookedSlotQty++;
                                eventSlotsBooked.put(oli.Type__c, bookedSlotQty);
                                bookedSlotsTotal.put(oli.IventoryItemtId__c, eventSlotsBooked);
                            }
                        }
                    } else {
                        //oli.addError('Your Inventory is not properly planned. Contact your administorator! No related Availability record found!');
                        oli.addError(System.Label.NoRelatedAvailabilityRecordFound_Error);
                    }
                } else {
                    oli.addError('Your Inventory is not properly planned. Contact your administorator! Check Availabilities on Inventory!');
                }          
            }
            // limited per event = false 
            else if (e != null && false == oli.LimitedPerEvent__c) {
                //load relevant maps for this line item from helper maps
                final Map<String,Availability__c> inventoryAvailability = availabilitiesTotal.get(oli.IventoryItemtId__c);
                
                if (inventoryAvailability != null) {

                    final Availability__c availablity = inventoryAvailability.get(oli.Type__c);
                    
                    system.debug('Availability record: '+availablity);
                    
                    if (null != availablity) {
                        
                        // BLACKLISTCHECK: only perform if blacklist for relevant Planned Slot is not switched off
                        if (true != availablity.No_Blacklist__c) {

                            final Set<String> blacklistsToCheck = new Set<String>{oli.Type__c};
                            if(availablity.SponsoringBlacklist__c !=null)
                            {
                                blacklistsToCheck.addAll(availablity.SponsoringBlacklist__c.split(';'));
                            }

                            system.debug('Opp line item Event: '+oli.IventoryItemtId__c);
                            system.debug('Event Type Accounts Characteristics Map: '+eventTypeAccountsCharacteristics);

                            // Map<plannedSlot.TYPE, Map<account.Characteristic, Set<AccountId>>>
                            final Map<String,Map<String,Set<Id>>> eventBlacklist = eventTypeAccountsCharacteristics.get(oli.IventoryItemtId__c);
                            
                            system.debug('eventBlacklist: ' + eventBlacklist);
                            system.debug('Account Industry: ' + a.Industry_Area__c);
                            system.debug('blacklistsToCheck: ' + blacklistsToCheck);
                            if (null != a.Industry_Area__c) {
                                for(String eventtype : blacklistsToCheck) {
                                    final Map<String,Set<Id>> typeBlacklist = eventBlacklist.get(eventtype);
                                    system.debug('typeBlacklist: ' + typeBlacklist);
                                    if(typeBlacklist !=null) {
                                        for(String accountChars : a.Industry_Area__c.split(';')) {
                                            Set<Id> accountIdsForBlacklist = typeBlacklist.get(accountChars);
                                            system.debug('accountIdsForBlacklist: ' + accountIdsForBlacklist);
                                            if(accountIdsForBlacklist != null && accountIdsForBlacklist.size() > 0 && typeBlacklist.keySet().contains(accountChars) && ! accountIdsForBlacklist.contains(a.id)) {
                                                //oli.addError('BLACKLIST: A customer with the industry '+ accountChars + ' has already booked a ' + eventtype + ' slot.');
                                                oli.addError(String.format(System.Label.BlackList_Validation_Error, new string[] {accountChars,eventtype}));
                                                break;
                                            }
                                        }                                
                                    }
                                }
                            }
                        }// END OF BLACKLISTCHECK
    
                        //IF BLACKLIST IS PASSED
                        //go ahead only if DeadLineDate is in the future or it's passed and this has been approved
                        //potential pitfall: if turtle wants to implement more than one approval process the util_approval might need different APPROVED STATUS values (e.g. "approved deadline", "approved margin")
                        if (e.Deadline_Date__c >= date.today()) {
                            final BookingOverview__c bookingToUpsert;
                            if (existingBookingOverview.containsKey(oli.id)) {
                                bookingToUpsert = existingBookingOverview.get(oli.id);
                            } else {
                                bookingToUpsert = new BookingOverview__c();
                            }

                            system.debug('opp line item: '+oli.Id);
                            system.debug('opp line item Type: '+oli.Type__c);
                            system.debug('opp line item status: '+oli.Status__c );
                            system.debug('Booking to upsert id: '+bookingToUpsert.id+' bookingToUpsert Status: '+bookingToUpsert.Status__c);                              
                           
                            // update SLOT fields from OppLineItem
                            bookingToUpsert.Quantity__c = oli.Quantity;
                            bookingToUpsert.Opportunity__c = oli.OpportunityId;
                            bookingToUpsert.Planned_Slot__c = availablity.id;
                            bookingToUpsert.OpportunityLineItemId__c = oli.id;
                            bookingToUpsert.Product__c = oli.Product2Id;
                            bookingToUpsert.InventoryItem__c = oli.IventoryItemtId__c;
                            bookingToUpsert.AdditionalCosts__c = oli.Additional_Costs__c ;
                            bookingToUpsert.GrossMargin__c = oli.Gross_Margin__c;
                            bookingToUpsert.ListPrice__c = oli.ListPrice;
                            bookingToUpsert.SalesPrice__c = oli.UnitPrice;
                            bookingToUpsert.TotalPrice__c = oli.TotalPrice;
                            bookingToUpsert.TotalCosts__c = oli.Total_Costs__c;
                            bookingToUpsert.Boothsize_in_sqm__c = oli.Boothsize_in_sqm__c;
                            bookingToUpsert.Status__c = oli.Status__c;
                            bookingToUpsert.Account__c = oli.AccountId__c;
                            bookingToUpsert.Type__c = oli.Type__c;
                            bookingToUpsert.LimitedPerInventory__c = false;
                            upsertSlots.add(bookingToUpsert);
                            System.debug('###' + bookingToUpsert);
                        }
                    } else {
                        //oli.addError('Your Inventory is not properly planned. Contact your administorator! No related Availability record found!');
                        oli.addError(System.Label.NoRelatedAvailabilityRecordFound_Error);
                    }
                }
            } 
            // inventory == null
            else {               
                oli.addError('Your Inventory is not properly planned. Contact your administorator!');
            }      
        }
        
        if(upsertSlots.size() > 0) upsert upsertSlots OpportunityLineItemId__c;
    }

}