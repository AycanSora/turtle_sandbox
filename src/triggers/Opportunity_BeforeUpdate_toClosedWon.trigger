trigger Opportunity_BeforeUpdate_toClosedWon on Opportunity (before update) {

    if(trigger.isUpdate) {
         List<Id> oppId = new List<Id>(trigger.newMap.keySet());
         List<OpportunityLineItem> oppLineItemList = [
                SELECT id, Status__c
                FROM OpportunityLineItem 
                WHERE OpportunityId 
                IN :oppId ];
        for(Opportunity opp : trigger.new) {
            if(opp.StageName == 'Closed Won') {
                for (OpportunityLineItem oli : oppLineItemList){
                    oli.status__c = 'Booked';
                }
            }
        }     
        try {
            upsert oppLineItemList id;
        } catch(DMLException e) {
            Opportunity actualRecord = Trigger.newMap.get(oppId[0]); 
            actualRecord.addError('Opportunity Closed Won Operation has failed! Re-check Opportunity Products!');
        }   
    }
}