@isTest
private class OppLineItemInvntryBO_AType_OppStge_Test {
   
    @testSetup public static void setup() { 
        List<Product2> products = new List<Product2>();
            products.add(new Product2(
                    Name = 'Product1',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Hospitality'));
            products.add(new Product2(
                    Name = 'Product2',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Hospitality'));
            products.add(new Product2(
                    Name = 'Product3',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Supplier'));
            products.add(new Product2(
                    Name = 'Product4',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Supplier'));
            products.add(new Product2(
                    Name = 'Product5',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Party'));
            products.add(new Product2(
                    Name = 'Product6',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Other'));
            products.add(new Product2(
                    Name = 'Product7',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Booth'));
            products.add(new Product2(
                    Name = 'Product8',
                    IsActive = true,
                    Limited_per_Event__c = true, 
                    Type__c = 'Booth'));
        products.sort();
        insert products; 

        List<PricebookEntry> pbEntries = new List<PricebookEntry>();
        
        for (Product2 product : products) {
            pbEntries.add(new PricebookEntry(
                    Pricebook2Id = Test.getStandardPricebookId(),
                    Product2Id = product.Id,
                    UnitPrice = 100.00,
                    IsActive = true));
        }
        insert pbEntries;     

        List<Inventory__c> testInventories = new List<Inventory__c>();      
            testInventories.add(new Inventory__c(
                    Name = 'testInventory1', 
                    Deadline_Date__c = date.today(),
                    Pricebook__c = Test.getStandardPricebookId()));
            testInventories.add(new Inventory__c(
                    Name = 'testInventory2', 
                    Deadline_Date__c = date.today(),
                    Pricebook__c = Test.getStandardPricebookId()));
            testInventories.add(new Inventory__c(
                    Name = 'testInventory3', 
                    Deadline_Date__c = date.today(),
                    Pricebook__c = Test.getStandardPricebookId()));
        testInventories.sort();
        insert testInventories;

        List<Availability__c> testAvailabilities = new List<Availability__c>();      
            testAvailabilities.add(new Availability__c( 
                    InventoryItem__c = testInventories[0].Id, 
                    Type__c = 'Hospitality',
                    Planned__c = 2,
                    SponsoringBlacklist__c = 'Hospitality;Supplier',
                    No_Blacklist__c = false));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[0].Id, 
                    Type__c = 'Supplier',
                    Planned__c = 1,
                    SponsoringBlacklist__c = 'Supplier',
                    No_Blacklist__c = false));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[0].Id, 
                    Type__c = 'Booth',
                    Planned__c = 100,
                    No_Blacklist__c = true));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[1].Id, 
                    Type__c = 'Hospitality',
                    Planned__c = 1,
                    SponsoringBlacklist__c = 'Hospitality',
                    No_Blacklist__c = false));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[1].Id, 
                    Type__c = 'Supplier',
                    Planned__c = 1,
                    No_Blacklist__c = true));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[1].Id, 
                    Type__c = 'Party',
                    Planned__c = 3,
                    SponsoringBlacklist__c = 'Party;Supplier',
                    No_Blacklist__c = false));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[2].Id, 
                    Type__c = 'Booth',
                    Planned__c = 600,
                    No_Blacklist__c = true));
            testAvailabilities.add(new Availability__c(
                    InventoryItem__c = testInventories[2].Id, 
                    Type__c = 'Other',
                    Planned__c = 1,
                    No_Blacklist__c = true));
        insert testAvailabilities;

        List<Account> testAccts = new List<Account>();      
            testAccts.add(new Account(
                    Name = 'TestAcct1', 
                    Industry_Area__c = 'Tablets;Toys',
                    Industry__c = 'Betting'));
            testAccts.add(new Account(
                    Name = 'TestAcct2', 
                    Industry_Area__c = 'Toys',
                    Industry__c = 'Betting'));
            testAccts.add(new Account(
                    Name = 'TestAcct3', 
                    Industry_Area__c = 'Tablets',
                    Industry__c = 'Betting'));
        testAccts.sort();
        insert testAccts;

        List<Opportunity> testOpps = new List<Opportunity>();      
            testOpps.add(new Opportunity(
                    Name = 'TestOpp1', 
                    CloseDate = date.today(), 
                    Account = testAccts[0], 
                    Type = 'New Business',
                    StageName = 'Proposal',
                    InventoryItem__c = testInventories[0].Id,
                    AccountId = testAccts[0].Id));
            testOpps.add(new Opportunity(
                    Name = 'TestOpp2', 
                    CloseDate = date.today(), 
                    Account = testAccts[1], 
                    Type = 'New Business',
                    StageName = 'Proposal',
                    InventoryItem__c = testInventories[1].Id,
                    AccountId = testAccts[1].Id));
            testOpps.add(new Opportunity(
                    Name = 'TestOpp3', 
                    CloseDate = date.today(), 
                    Account = testAccts[2], 
                    Type = 'New Business',
                    StageName = 'Proposal',
                    InventoryItem__c = testInventories[1].Id,
                    AccountId = testAccts[2].Id));
            testOpps.add(new Opportunity(
                    Name = 'TestOpp4', 
                    CloseDate = date.today(), 
                    Account = testAccts[2], 
                    Type = 'New Business',
                    StageName = 'Proposal',
                    InventoryItem__c = testInventories[2].Id,
                    AccountId = testAccts[2].Id));
            testOpps.add(new Opportunity(
                    Name = 'TestOpp5', 
                    CloseDate = date.today(), 
                    Account = testAccts[2], 
                    Type = 'New Business',
                    StageName = 'Proposal',
                    InventoryItem__c = testInventories[0].Id,
                    AccountId = testAccts[2].Id));
        testOpps.sort();
        insert testOpps;

        
    }

    @isTest public static void add_Delete_OppLineItems() {
    
        List<PricebookEntry> priceBookEntities = [ SELECT Id FROM PricebookEntry];       
        List<Opportunity> opps = [ SELECT Id, Name, AccountId, InventoryItem__c FROM Opportunity ORDER BY Name];
        List<Inventory__c> inventories = [ SELECT Id, Name FROM Inventory__c ORDER BY Name];
        List<Availability__c> availabilities = [ SELECT Id, InventoryItem__c, Booked__c, Planned__c, Type__c FROM Availability__c];
                
        OpportunityLineItem opplineItem1 = new OpportunityLineItem();
                opplineItem1.PricebookEntryId = priceBookEntities[0].Id;
                opplineItem1.Opportunity = opps[0];
                opplineItem1.OpportunityId = opps[0].Id;
                opplineItem1.Quantity = 1;
                opplineItem1.Status__c = 'Optional';
                opplineItem1.UnitPrice = 10;
        insert opplineItem1;
        
        List<BookingOverview__c> bookingOverviews = [
                SELECT 
                    Id, Account__c, InventoryItem__c, InventoryItem__r.Name, Opportunity__c, OpportunityLineItemId__c, Planned_Slot__c,
                    Product__c, Status__c, Type__c, Quantity__c
                FROM 
                    BookingOverview__c];  
      
        system.assertEquals(availabilities[0].Id, bookingOverviews[0].Planned_Slot__c);
        system.assertEquals('Hospitality', bookingOverviews[0].Type__c);
        system.assertEquals('testInventory1', bookingOverviews[0].InventoryItem__r.Name);
        system.assertEquals(1, bookingOverviews[0].Quantity__c);
    
        opps[0].Stagename = 'Closed Won';
        Update Opps;
        OpportunityLineItem afterInsertOppLineItem = [SELECT Id, OpportunityId, Status__c, IventoryItemtId__c 
                                            FROM OpportunityLineItem 
                                            WHERE Id = :opplineItem1.Id limit 1];
        
        availabilities = [ SELECT Id, InventoryItem__c, Booked__c, Planned__c, Type__c FROM Availability__c];
        bookingOverviews = [ SELECT Id, Account__c, InventoryItem__c, InventoryItem__r.Name, Opportunity__c, OpportunityLineItemId__c, Planned_Slot__c,
                                    Product__c, Status__c, Type__c, Quantity__c
                            FROM BookingOverview__c]; 
        
        system.assertEquals(1, availabilities[0].Booked__c);
        system.assertEquals('Booked', bookingOverviews[0].Status__c);
        
        try{
        OpportunityLineItem opplineItem2 = new OpportunityLineItem();
                opplineItem2.PricebookEntryId = priceBookEntities[1].Id;
                opplineItem2.Opportunity = opps[4];
                opplineItem2.OpportunityId = opps[4].Id;
                opplineItem2.Quantity = 1;
                opplineItem2.Status__c = 'Optional';
                opplineItem2.UnitPrice = 10;
            insert opplineitem2;
             System.assert(false, 'Exception expected.');
        } catch (DmlException e) {
            Boolean expectedExceptionThrown = e.getMessage().contains
                ('BLACKLIST: A customer with the industry') ? true : false;  
                System.assertEquals(expectedExceptionThrown, true);
        }

        OpportunityLineItem afterFail_GetOppLineItem1 = [SELECT Id, OpportunityId, Status__c, IventoryItemtId__c 
                                                            FROM OpportunityLineItem 
                                                            WHERE Id = :opplineItem1.Id limit 1];

        delete afterFail_GetOppLineItem1;

        bookingOverviews = [SELECT Id, Account__c, InventoryItem__c, InventoryItem__r.Name, Opportunity__c, OpportunityLineItemId__c, Planned_Slot__c,
                                    Product__c, Status__c, Type__c, Quantity__c
                            FROM  BookingOverview__c];  
        system.assertEquals('Lost', bookingOverviews[0].Status__c);

        OpportunityLineItem opplineItem3 = new OpportunityLineItem();
                opplineItem3.PricebookEntryId = priceBookEntities[2].Id;
                opplineItem3.Opportunity = opps[1];
                opplineItem3.OpportunityId = opps[1].Id;
                opplineItem3.Quantity = 1;
                opplineItem3.Status__c = 'Optional';
                opplineItem3.UnitPrice = 10;
        insert opplineItem3;

        OpportunityLineItem opplineItem4 = new OpportunityLineItem();
                opplineItem4.PricebookEntryId = priceBookEntities[3].Id;
                opplineItem4.Opportunity = opps[2];
                opplineItem4.OpportunityId = opps[2].Id;
                opplineItem4.Quantity = 1;
                opplineItem4.Status__c = 'Optional';
                opplineItem4.UnitPrice = 10;
        insert opplineItem4;

    }

    @isTest static void oppStage_ClosedWon() {
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT id, Stagename FROM Opportunity]);
        List<OpportunityLineItem> oppLineItemList = [
                SELECT id, Status__c
                FROM OpportunityLineItem 
                WHERE OpportunityId 
                IN :oppMap.keyset() ];
        try {
            for ( Opportunity opp : oppMap.values()) {
                    opp.StageName = 'Closed Won';
                for (OpportunityLineItem oli : oppLineItemList){
                     oli.status__c = 'Booked';
                }
            }
            upsert oppMap.values();
            upsert oppLineItemList;
        } catch(DmlException e) {
             Boolean expectedExceptionThrown = e.getMessage().contains
                ('Opportunity Closed Won Operation has failed') ? true : false;  
                System.assertEquals(expectedExceptionThrown, true);
        }
    }
    
    @isTest static void add_Same_Type_Availability_Record() {
        List<Inventory__c> inventories = [ SELECT Id, Name FROM Inventory__c ORDER BY Name];
        List<Availability__c> availabilities = [ SELECT Id, InventoryItem__c, Booked__c, Planned__c, Type__c 
                                                 FROM Availability__c];
        try {
            availabilities.add(
                new Availability__c (
                    Type__c = 'Hospitality', 
                    Planned__c = 1, 
                    InventoryItem__c = inventories[0].Id));
            upsert availabilities;
        } catch(DmlException e) {
             Boolean expectedExceptionThrown = e.getMessage().contains
                ('The type') ? true : false;  
                System.assertEquals(expectedExceptionThrown, true);
        }
    }
   
}